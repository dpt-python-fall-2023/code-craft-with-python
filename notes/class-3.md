# Заняття 3. Функції. Написання функцій, диаграми стеку. Результативні функції та void функції.

* [Визначення функції](#functions)
* [Додавання власних функцій](#add-functions)
* [Порядок виконання програми](#execution-flow)
* [Аргументи та параметри в функціях](#arguments-and-local-variables)
* [Аргументи та параметри в функціях](#arguments-and-local-variables)
* [Діаграми стеку](#stack-diagrams)
* [Результативні функції та void функції](#fruitful-and-void-functions)
* [Вправи](#exercises)

## <a name="functions">Визначення функції</a>
У контексті програмування _**функція**_ - це іменована послідовність операторів, яка виконує обчислення.
Коли ви визначаєте функцію, ви вказуєте ім'я та послідовність операторів. Пізніше ви можете "викликати" функцію за назвою.

Ми вже бачили приклади виклику функції:

```
>>> type(42)
class 'int'
```

Розберемо цей приклад. Ім'я функції - `type`. Вираз у круглих дужках називається аргументом функції. Результатом для цієї функції є тип аргументу.
Зазвичай кажуть, що функція "отримує" аргумент і "повертає" результат. Результат також називається значенням, що повертається.

## <a name="add-functions">Додавання власних функцій</a>

Для того, щоб додати нову функцію, необхідно вказати ім'я нової функції та послідовність операторів, які виконуються при її виклику.

Приклад:

```
def print_dialog():
    print("- Тарас, а шо по рсні?")
    print("- Рсні п$зда")
```

_**def**_ - ключове слово, яке вказує на те, що це визначення функції. Ім'я функції - `print_dialog`.
Правила для назв функцій такі ж, як і для назв змінних: літери, цифри і символ підкреслення є допустимими, але першим символом не може бути цифра.
Ви не можете використовувати ключове слово як ім'я функції, і вам слід уникати створення змінної і функції з однаковими іменами.
Порожні дужки після імені означають, що ця функція не приймає жодних аргументів.

Перший рядок визначення функції називається заголовком, решта - тілом. Заголовок повинен закінчуватися двокрапкою, а тіло - з відступом.
За домовленістю, відступ завжди дорівнює чотирьом пробілам. Тіло може містити будь-яку кількість операторів.

Визначення функції створює об'єкт функції, який має тип function:

```
>>> print(print_dialog)
function print_dialog at 0xb7e99e9c
>>> type(print_dialog)
class 'function'
```

Синтаксис виклику нової функції такий самий, як і для вбудованих функцій:

```
>>> print_dialog()
```

Після того, як ви визначили функцію, ви можете використовувати її всередині іншої функції, наприклад:

```
def poplava():
    print('Поплава №1:')
    print_dialog()
    print('Поплава №2:')
    print_dialog()
```

Функція обов'язково має бути визначена перед тим, як викликана, інакше ви отримаєте помилку `NameError`.

## <a name="execution-flow">Порядок виконання програми</a>

Щоб переконатися, що функція визначена до її першого використання, ви повинні знати порядок виконання операторів, який називається потоком виконання.

Виконання завжди починається з першого оператора програми. Оператори виконуються по одному, у порядку зверху вниз.

Визначення функцій не змінюють потік виконання програми, але пам'ятайте, що оператори всередині функції не виконуються, поки функція не буде викликана.

Виклик функції - це як обхід у потоці виконання програми. Замість того, щоб перейти до наступного оператора,
потік перескакує до тіла функції, виконує оператори там, а потім повертається назад, щоб продовжити з того місця, де він зупинився.

Це звучить досить просто, доки ви не згадаєте, що одна функція може викликати іншу.
Під час виконання однієї функції програмі може знадобитися виконати оператори в іншій функції.
А потім, під час виконання цієї нової функції, програмі може знадобитися виконати ще одну функцію!

На щастя, Python добре відстежує, де вона знаходиться.
Тому кожного разу, коли функція завершується, програма продовжує виконання з того місця, де вона зупинилася у функції, яка її викликала.
Коли програма доходить до кінця, вона завершується.

Підсумовуючи, коли ви читаєте програму, вам не завжди потрібно читати зверху вниз.
Іноді має більше сенсу, якщо ви будете слідувати за ходом виконання.

## <a name="arguments-and-local-variables">Аргументи та параметри в функціях</a>

Деякі з функцій, які ми розглянули, вимагають аргументів.
Наприклад, коли ви викликаєте `math.sin`, ви передаєте число як аргумент.
Деякі функції приймають більше одного аргументу: `math.pow` приймає два - основу і показник степеня.

Усередині функції аргументи присвоюються змінним, які називаються параметрами. Нижче наведено опис функції, яка отримує аргумент:

```
def print_twice(spam):
    print(spam)
    print(spam)
```

Ця функція присвоює аргумент параметру на ім'я `spam`. Коли функція викликається, вона двічі друкує значення параметра (яким би він не був).
Ця функція працює з будь-яким значенням, яке можна надрукувати.

```
>>> print_twice('Спам')
Спам
Спам
>>> print_twice(42)
42
42
>>> print_twice(math.pi)
3.14159265359
3.14159265359
```

Коли ви створюєте змінну всередині функції, вона є локальною, тобто існує лише всередині функції. Наприклад

```
def cat_twice(part1, part2):
    cat = part1 + part2
    print_twice(cat)
```
Ця функція отримує два аргументи, об'єднує їх і двічі друкує результат. Коли `cat_twice` завершується, змінна `cat` знищується.
Якщо ми спробуємо надрукувати її, то отримаємо помилку:
```
>>> print(cat)
NameError: name 'cat' is not defined
```
Параметри також є локальними. Наприклад, за межами `print_twice` не існує такого поняття як `spam`.

## <a name="stack-diagrams">Діаграми стеку</a>

Щоб відстежувати, які змінні де використовуються, іноді корисно намалювати діаграму стека.
Як і діаграми станів, стекові діаграми показують значення кожної змінної, але вони також показують функцію, до якої належить кожна змінна.

Кожна функція представлена фреймом. Фрейм - це рамка з назвою функції поруч з нею та параметрами і змінними функції всередині неї.

<demo>

Фрейми розташовані у стеку, який вказує, яка функція яку викликала, і так далі. Найвищий фрейм називається `__main__`.

Якщо під час виклику функції виникає помилка, Python виводить ім'я функції, ім'я функції, яка її викликала, і ім'я функції, яка викликала цю функцію, аж до `__main__`.

Наприклад, якщо ви спробуєте отримати доступ до `cat` зсередини `print_twice`, ви отримаєте `NameError`:

```
Traceback (innermost last):
  File "test.py", line 13, in __main__
    cat_twice(line1, line2)
  File "test.py", line 5, in cat_twice
    print_twice(cat)
  File "test.py", line 9, in print_twice
    print(cat)
NameError: name 'cat' is not defined
```
Цей список функцій називається _traceback_.
Він показує, в якому програмному файлі виникла помилка, в якому рядку і які функції виконувалися в цей час.
Він також показує рядок коду, який спричинив помилку.
Порядок функцій в traceback збігається з порядком фреймів на діаграмі стеку.
Функція, яка виконується в даний момент, знаходиться внизу.

## <a name="fruitful-and-void-functions">Результативні функції та void функціх</a>
Деякі з функцій, які ми використовували, наприклад, математичні функції, повертають результати.
За браком кращої назви я називаю їх _**результативними**_ функціями.
Інші функції, такі як `print_twice`, виконують дію, але не повертають значення. Вони називаються _**void**_ функціями.

## <a name="exercises">Вправи</a>

### Вправа 1
Напишіть функцію з ім'ям `right_justify`, яка отримує рядок з ім'ям `s` як параметр і виводить його з достатньою кількістю початкових пробілів, щоб остання літера рядка була у стовпчику 70 виведення.
```
>>> right_justify('monty')
                                                                 monty
```
Підказка: Використовуйте конкатенацію та повторення рядків. Також у `Python` є вбудована функція `len`, яка повертає довжину рядка, тому значення `len('monty')` дорівнює 5.

### Вправа 2

**Примітка: Для виконання цієї вправи слід використовувати лише оператори та інші функції, які ми вже вивчили.**

Напишіть функцію, яка малює сітку, подібну до наступної:
```
+ - - - - + - - - - +
|         |         |
|         |         |
|         |         |
+ - - - - + - - - - +
|         |         |
|         |         |
|         |         |
|         |         |
+ - - - - + - - - - +
```
_Підказка_: щоб вивести більше одного значення в рядку, ви можете вивести послідовність значень, розділених комами:
```
print('+', '-')
```
За замовчуванням, при друкуванні відбувається перехід на наступний рядок, але ви можете змінити цю поведінку і поставити пробіл в кінці, наприклад, так:
```
print('+', end=' ')
print('-')
```
У результаті виконання цих операторів буде виведено '+ -' у тому самому рядку. Виведення з наступного оператора друку почнеться з наступного рядка.

### Вправа 3

Напишіть функцію, яка малює подібну до вправи 2 сітку з чотирма рядками і чотирма стовпчиками.
